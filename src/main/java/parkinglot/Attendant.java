package parkinglot;

import parkinglot.parkingStrategy.AttendantParkStrategy;
import parkinglot.parkingStrategy.AttendantParkStrategyCreator;

import java.util.Arrays;
import java.util.List;

public class Attendant {

    private List<ParkingLot> parkingLots;
    private AttendantParkStrategy attendantParkStrategy;

    public Attendant(ParkingLot... parkingLots) {
        this(AttendantParkStrategy.ROUND_ROBIN_STRATEGY, parkingLots);
    }

    public Attendant(AttendantParkStrategyCreator attendantParkStrategyCreator, ParkingLot... parkingLots) {
        this.parkingLots = Arrays.asList(parkingLots);
        this.attendantParkStrategy = attendantParkStrategyCreator.apply(this.parkingLots);
    }


    public void park(Parkable parkable) {
        this.attendantParkStrategy.park(parkable);
    }

    public void unPark(Parkable parkable) {
        ParkingLot parkingLotOfParkable = this.parkingLots.stream()
                .filter((parkingLot) -> parkingLot.isCarParked(parkable))
                .findFirst()
                .orElseThrow(() -> {
                    throw new ParkableNotFoundException();
                });

        parkingLotOfParkable.unPark(parkable);
    }
}
