package parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ParkingLot {
    private HashSet<Parkable> parkedCars = new HashSet<>();
    private int parkingLotSize;
    private List<ParkingLotSpaceListener> parkingLotSpaceListener;

    public ParkingLot(int parkingLotSize, ParkingLotSpaceListener... parkingLotSpaceListener) {
        this.parkingLotSize = parkingLotSize;
        this.parkingLotSpaceListener = new ArrayList<>(Arrays.asList(parkingLotSpaceListener));
    }

    public void park(Parkable parkable) {
        if(parkedCars.size() == parkingLotSize) {
            throw new ParkingLotFullException();
        }

        parkedCars.add(parkable);

        checkAndNotifyListenersLotIsFull();
    }

    private void checkAndNotifyListenersLotIsFull() {
        if(parkedCars.size() == parkingLotSize) {
            parkingLotSpaceListener.forEach(
                    (parkingLotSpaceListener) -> parkingLotSpaceListener.notifyParkingLotFull(this));
        }
    }

    public boolean isCarParked(Parkable parkable) {
        return parkedCars.contains(parkable);
    }

    public void unPark(Parkable parkable) {
        parkedCars.remove(parkable);
        notifyListenersSpaceIsAvailable();
    }

    private void notifyListenersSpaceIsAvailable() {
        parkingLotSpaceListener.forEach(
                (parkingLotSpaceListener) -> parkingLotSpaceListener.notifyParkingLotSpaceAvailable(this)
        );
    }

    public void addSpaceListener(ParkingLotSpaceListener parkingLotSpaceListener) {
        this.parkingLotSpaceListener.add(parkingLotSpaceListener);
    }

    public int availableSpaces() {
        return this.parkingLotSize - this.parkedCars.size();
    }
}
