package parkinglot;

public interface ParkingLotSpaceListener {
    void notifyParkingLotFull(ParkingLot parkingLot);
    void notifyParkingLotSpaceAvailable(ParkingLot parkingLot);
}
