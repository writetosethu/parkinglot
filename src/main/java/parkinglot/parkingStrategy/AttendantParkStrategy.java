package parkinglot.parkingStrategy;

import parkinglot.Parkable;

public interface AttendantParkStrategy {
    void park(Parkable parkable);

    AttendantParkStrategyCreator ROUND_ROBIN_STRATEGY = RoundRobinParkingStrategy::new;
    AttendantParkStrategyCreator FILL_UP_LOT_STRATEGY = FillUpLotParkingStrategy::new;
    AttendantParkStrategyCreator FREE_SPACE_STRATEGY = FreeSpaceFirstStrategy::new;
}

