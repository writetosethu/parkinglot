package parkinglot.parkingStrategy;

import parkinglot.ParkingLot;
import parkinglot.parkingStrategy.AttendantParkStrategy;

import java.util.List;
import java.util.function.Function;

public interface AttendantParkStrategyCreator extends Function<List<ParkingLot>, AttendantParkStrategy> {}
