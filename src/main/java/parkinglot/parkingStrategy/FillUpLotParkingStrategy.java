package parkinglot.parkingStrategy;

import parkinglot.Parkable;
import parkinglot.ParkingLot;
import parkinglot.ParkingLotFullException;
import parkinglot.ParkingLotSpaceListener;

import java.util.LinkedList;
import java.util.List;

public class FillUpLotParkingStrategy implements AttendantParkStrategy, ParkingLotSpaceListener {

    private LinkedList<ParkingLot> availableParkingLots;
    private LinkedList<ParkingLot> fullParkingLots;

    public FillUpLotParkingStrategy(List<ParkingLot> parkingLots) {
        this.availableParkingLots = new LinkedList<>(parkingLots);
        this.fullParkingLots = new LinkedList<>();
        this.availableParkingLots.forEach(parkingLot -> parkingLot.addSpaceListener(this));
    }

    @Override
    public void park(Parkable parkable) {
        if(this.availableParkingLots.isEmpty()) {
            throw new ParkingLotFullException();
        }
        this.availableParkingLots.getFirst().park(parkable);
    }

    @Override
    public void notifyParkingLotFull(ParkingLot parkingLot) {
        this.availableParkingLots.remove(parkingLot);
        this.fullParkingLots.add(parkingLot);
    }

    @Override
    public void notifyParkingLotSpaceAvailable(ParkingLot parkingLot) {
        this.availableParkingLots.add(parkingLot);
        this.fullParkingLots.remove(parkingLot);
    }
}
