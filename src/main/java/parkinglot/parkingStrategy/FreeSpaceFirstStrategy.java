package parkinglot.parkingStrategy;

import parkinglot.Parkable;
import parkinglot.ParkingLot;

import java.util.Comparator;
import java.util.List;

public class FreeSpaceFirstStrategy implements AttendantParkStrategy {

    private List<ParkingLot> parkingLots;

    public FreeSpaceFirstStrategy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public void park(Parkable parkable) {
        parkingLots.sort(Comparator.comparingInt(ParkingLot::availableSpaces).reversed());
        parkingLots.get(0).park(parkable);
    }
}
