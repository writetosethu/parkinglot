package parkinglot.parkingStrategy;

import parkinglot.Parkable;
import parkinglot.ParkingLot;

import java.util.List;

class RoundRobinParkingStrategy implements AttendantParkStrategy {

    private List<ParkingLot> parkingLots;
    private int indexOfLastParkedLot = 0;

    public RoundRobinParkingStrategy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public void park(Parkable parkable) {
        getParkingLotToPark().park(parkable);
        updateIndexToUseForNextCar();
    }

    private ParkingLot getParkingLotToPark() {
        return this.parkingLots.get(indexOfLastParkedLot);
    }

    private void updateIndexToUseForNextCar() {
        indexOfLastParkedLot = (indexOfLastParkedLot + 1) % parkingLots.size();
    }
}
