package parkinglot;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class AttendantTest {

    @Test
    @DisplayName("should park car in parking lot")
    public void shouldParkCar() {
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, parkingLotSpaceListener);

        Attendant attendant = new Attendant(parkingLot);

        Car car = new Car();
        attendant.park(car);

        assertTrue(parkingLot.isCarParked(car));
    }

    @Test
    @DisplayName("should unPark car from the right parking lot")
    public void shouldUnParkCar() {
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot1 = new ParkingLot(2, parkingLotSpaceListener);
        ParkingLot parkingLot2 = new ParkingLot(2, parkingLotSpaceListener);
        Attendant attendant = new Attendant(parkingLot1, parkingLot2);

        Car car1 = new Car();
        attendant.park(car1);
        Car car2 = new Car();
        attendant.park(car2);

        attendant.unPark(car2);

        assertFalse(parkingLot2.isCarParked(car2));
    }

    @Test
    @DisplayName("should throw an error when car is not parked in any parking lot and we try to unpark")
    public void shouldThrowExceptionWhenParkCar() {
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot1 = new ParkingLot(2, parkingLotSpaceListener);
        Attendant attendant = new Attendant(parkingLot1);

        assertThrows(ParkableNotFoundException.class, () -> attendant.unPark(new Car()));
    }

}