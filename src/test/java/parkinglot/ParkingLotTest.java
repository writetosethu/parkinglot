package parkinglot;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class ParkingLotTest {

    @Test
    @DisplayName("should park the car in the parking lot")
    public void testCarParked1() {
        ParkingLotSpaceListener listener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, listener);
        Car car = new Car();

        parkingLot.park(car);

        assertTrue(parkingLot.isCarParked(car));
    }

    @Test
    @DisplayName("should return false for a car that is not parked in the parking lot")
    public void testCarNotParked() {
        ParkingLotSpaceListener listener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, listener);
        Car car = new Car();
        Car unparkedCar = new Car();

        parkingLot.park(car);

        assertFalse(parkingLot.isCarParked(unparkedCar));
    }

    @Test
    @DisplayName("should un-park a car from the parkinglot")
    public void unparkCar() {
        ParkingLotSpaceListener listener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, listener);
        Car car = new Car();

        parkingLot.park(car);
        parkingLot.unPark(car);

        assertFalse(parkingLot.isCarParked(car));
    }

    @Test
    @DisplayName("should notify parking lot owner when car park is full")
    public void notifyOwner() {
        ParkingLotSpaceListener parkingLotOwner = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, parkingLotOwner);
        Car car = new Car();

        parkingLot.park(car);

        verify(parkingLotOwner).notifyParkingLotFull(parkingLot);
    }

    @Test
    @DisplayName("should not notify parking lot owner when car park is not full")
    public void dontNotifyOwner() {
        ParkingLotSpaceListener parkingLotOwner = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(2, parkingLotOwner);
        Car car = new Car();

        parkingLot.park(car);

        verify(parkingLotOwner, times(0)).notifyParkingLotFull(parkingLot);
    }


    @Test
    @DisplayName("should throw an exception when parking a car into a full car park")
    public void expectCarFullException() {
        ParkingLotSpaceListener parkingLotOwner = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, parkingLotOwner);
        parkingLot.park(new Car());

        assertThrows(ParkingLotFullException.class, () -> parkingLot.park(new Car()));
    }

    @Test
    @DisplayName("should notify owner and security personnel when parking lot is full")
    public void notifyBoth() {
        ParkingLotSpaceListener parkingLotOwner = mock(ParkingLotSpaceListener.class);
        ParkingLotSpaceListener securityPersonal = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, parkingLotOwner, securityPersonal);

        parkingLot.park(new Car());

        verify(parkingLotOwner).notifyParkingLotFull(parkingLot);
        verify(securityPersonal).notifyParkingLotFull(parkingLot);
    }

    @Test
    @DisplayName("should notify listeners when space is available")
    public void notifySpaceAvailable(){
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1, parkingLotSpaceListener);

        Car parkable = new Car();
        parkingLot.park(parkable);
        parkingLot.unPark(parkable);

        verify(parkingLotSpaceListener).notifyParkingLotSpaceAvailable(parkingLot);
    }

    @Test
    @DisplayName("should notify additional listeners if space is full")
    public void addAdditionalListeners(){
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot = new ParkingLot(1);

        parkingLot.addSpaceListener(parkingLotSpaceListener);

        Car parkable = new Car();
        parkingLot.park(parkable);

        verify(parkingLotSpaceListener).notifyParkingLotFull(parkingLot);
    }

    @Test
    @DisplayName("should get 2 as available space")
    public void availableSpace(){
        ParkingLot parkingLot = new ParkingLot(2);
        parkingLot.park(new Car());

        assertEquals(1, parkingLot.availableSpaces());
    }
}
