package parkinglot.parkingStrategy;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import parkinglot.Attendant;
import parkinglot.Car;
import parkinglot.ParkingLot;
import parkinglot.ParkingLotFullException;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FillUpLotParkingStrategyTest {

    @Test
    @DisplayName("should park car in first available lot")
    public void shouldParkCarRoundRobin() {
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);

        Attendant attendant = new Attendant(AttendantParkStrategy.FILL_UP_LOT_STRATEGY, parkingLot1, parkingLot2);

        Car car1 = new Car();
        attendant.park(car1);

        Car car2 = new Car();
        attendant.park(car2);

        Car car3 = new Car();
        attendant.park(car3);

        assertTrue(parkingLot1.isCarParked(car1));
        assertTrue(parkingLot1.isCarParked(car2));
        assertTrue(parkingLot2.isCarParked(car3));
    }

    @Test
    @DisplayName("should throw an exception if space is not available in any lot")
    public void showThrowException() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);

        Attendant attendant = new Attendant(AttendantParkStrategy.FILL_UP_LOT_STRATEGY, parkingLot1, parkingLot2);

        attendant.park(new Car());
        attendant.park(new Car());

        assertThrows(ParkingLotFullException.class, () -> attendant.park(new Car()));
    }

}