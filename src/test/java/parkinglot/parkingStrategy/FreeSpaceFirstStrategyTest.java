package parkinglot.parkingStrategy;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import parkinglot.Attendant;
import parkinglot.Car;
import parkinglot.ParkingLot;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FreeSpaceFirstStrategyTest {

    @Test
    @DisplayName("should park car in first available lot")
    public void shouldParkCarRoundRobin() {
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(4);

        Attendant attendant = new Attendant(AttendantParkStrategy.FREE_SPACE_STRATEGY, parkingLot1, parkingLot2);

        List<Car> cars = Arrays.asList(new Car(), new Car(), new Car(), new Car(), new Car());
        cars.forEach(attendant::park);

        assertTrue(parkingLot2.isCarParked(cars.get(0)));
        assertTrue(parkingLot2.isCarParked(cars.get(1)));
        assertTrue(parkingLot2.isCarParked(cars.get(2)));
        assertTrue(parkingLot1.isCarParked(cars.get(3)));
        assertTrue(parkingLot1.isCarParked(cars.get(4)));
    }
}