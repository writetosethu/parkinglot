package parkinglot.parkingStrategy;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import parkinglot.Attendant;
import parkinglot.Car;
import parkinglot.ParkingLot;
import parkinglot.ParkingLotSpaceListener;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class RoundRobinParkingStrategyTest {

    @Test
    @DisplayName("should park car in a round robin manner")
    public void shouldParkCarRoundRobin() {
        ParkingLotSpaceListener parkingLotSpaceListener = mock(ParkingLotSpaceListener.class);
        ParkingLot parkingLot1 = new ParkingLot(2, parkingLotSpaceListener);
        ParkingLot parkingLot2 = new ParkingLot(2, parkingLotSpaceListener);

        Attendant attendant = new Attendant(AttendantParkStrategy.ROUND_ROBIN_STRATEGY, parkingLot1, parkingLot2);

        Car car1 = new Car();
        attendant.park(car1);

        Car car2 = new Car();
        attendant.park(car2);

        Car car3 = new Car();
        attendant.park(car3);

        assertTrue(parkingLot1.isCarParked(car1));
        assertTrue(parkingLot2.isCarParked(car2));
        assertTrue(parkingLot1.isCarParked(car3));
    }


}